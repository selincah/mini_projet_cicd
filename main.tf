provider "aws" {
    region = "ca-central-1"
    access_key = "AKIAXXXNTM5FFDUVUC7Z"
    secret_key = "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"
}

data "aws_ami" "ami_aws_linux" {
  most_recent      = true
  owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_security_group" "security_group" {
  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  
}

resource "aws_instance" "server_jenkins" {
    ami = data.aws_ami.ami_aws_linux.id
    instance_type = "t2.micro"
    key_name = "Selinca"
    associate_public_ip_address = true
    vpc_security_group_ids = [aws_security_group.security_group.id]
    tags = {
        Name = "Jenkins_server_selin"
        Owner = "Selin"
    }
    root_block_device {
      delete_on_termination = true
  }

}
resource "aws_instance" "server_docker" {
    ami = data.aws_ami.ami_aws_linux.id
    instance_type = "t2.micro"
    key_name = "Selinca"
    associate_public_ip_address = true
    vpc_security_group_ids = [aws_security_group.security_group.id]
    tags = {
        Name = "Docker_server_selin"
        Owner = "Selin"
    }
    root_block_device {
      delete_on_termination = true
  }

}