resource "local_file" "AnsibleInventory" {
 content = templatefile("inventory.tmpl",
 {
  jenkins-dns = aws_instance.server_jenkins.public_dns,
  jenkins-ip = aws_instance.server_jenkins.public_ip,
  jenkins-id = aws_instance.server_jenkins.id,
  docker-dns = aws_instance.server_docker.public_dns,
  docker-ip = aws_instance.server_docker.public_ip,
  docker-id = aws_instance.server_docker.id
 }
 )
 filename = "inventory"
}